FROM ej52/alpine-nginx-php:latest
MAINTAINER Leon Harvey "https://github.com/neoh"

run mkdir /home/crons

ADD etc /etc/
ADD error_pages /usr/share/nginx/html/
ADD envplate /home/
ADD start.sh /etc/cont-init.d/nx_start
ADD crons /home/crons/


run \
    chmod +x /home/envplate && \
    chmod -R +x /home/crons && \
    chmod +x /etc/cont-init.d/nx_start && \
    crontab -l > /home/crons/new_cron && \
    echo "5 8 * * Sun /bin/sh /home/crons/weekly.sh" >> /home/crons/new_cron && \
    crontab /home/crons/new_cron && \
    rm /home/crons/new_cron && \
    apk update && \
    apk add --update autoconf gcc g++ && \
    printf "\n" | pecl install apcu && \
    echo extension=apcu.so > /etc/php/conf.d/apcu.ini

EXPOSE 80 443

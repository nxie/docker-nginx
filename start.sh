#!/usr/bin/with-contenv sh
adduser -D www
/home/envplate /etc/nginx/conf.d/default.conf
mkdir /var/www/tmp
mkdir /var/log

chmod 777 /var/www

find /var/www -type f -exec chmod 755 {} +
find /var/www -type d -exec chmod 755 {} +

chgrp -R www-data /var/www
chgrp -R www-data /var/log
chgrp -R www-data /var/www/tmp

chown -R www-data:www-data /var/www
chown -R www-data:www-data /var/log
chown -R www-data:www-data /var/www/tmp

chmod 440 /var/www/wp-config.php
chmod -R 755 /var/www/tmp
chmod -R 440 /var/log

chown www-data:www-data /var/lib/php7/sessions
chmod 755 /var/lib/php7/sessions 